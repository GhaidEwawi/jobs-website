<?php
    session_start();
    require "constants.php";
    require "$BASE_URL/database/connection.php";

    unset($_SESSION['logged-in']);
    unset($_SESSION['logged-in-permission']);
    header('Location:' . $BASE_URL);
?>