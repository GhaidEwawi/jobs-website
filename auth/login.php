<?php
    session_start();
    require "constants.php";
    require "$BASE_URL/database/connection.php";
    require "$BASE_URL/database/db.php";
    require "$BASE_URL/data/users.php";

    $username = mysqli_real_escape_string($conn, $_POST['username']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);

    $user = findUser($username, $password);

    if ( !empty($user) ) {
        $_SESSION['logged-in'] = $user['id'];
        $_SESSION['logged-in-permission'] = $user['role'];
        header("Location: $BASE_URL/index.php");
    }
    else {
        header("Location: $BASE_URL/pages/login.php?msg=error");
    }
?>