<?php

    session_start();
    require "constants.php";
    require "$BASE_URL/database/connection.php";
    require "$BASE_URL/database/db.php";
    require "$BASE_URL/data/users.php";

    $id = $_GET['id'];
    
    $result = deleteUserById($id);

    if($result) {
        echo "User Deleted Successfully";
    } else {
        echo "Error Deleting User";
    }

?>