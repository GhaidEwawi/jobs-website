<?php

    function getAllJobOffers() {
        $query = "SELECT * 
                  FROM offers";
        $conn = connect();
        $result = mysqli_query($conn, $query);
        
        $offers = [];

        while ($row = mysqli_fetch_assoc($result)) {
            $offers[] = $row;
        }
        
        return $offers;
    }

    function getSponseredJobOffers() {
        
        $query = "SELECT offers.*
                  FROM offers AS offers
                  JOIN sponsered AS spon on spon.offerId = offers.id
                  ORDER BY spon.price DESC
                  LIMIT 6";
        
        $conn = connect();
        $result = mysqli_query($conn, $query);
        
        $offers = [];

        while ($row = mysqli_fetch_assoc($result)) {
            $offers[] = $row;
        }
        
        return $offers;
    }

    function getOfferById($id) {
        
        $query = "SELECT * 
                  FROM offers 
                  WHERE id = $id";
        
        $conn = connect();
        $result = mysqli_query($conn, $query);
        $offer = mysqli_fetch_assoc($result);
        
        return $offer;
    }

    function getOffersByUserId($userId) {
        
        $query = "SELECT * 
                  FROM offers 
                  WHERE id IN (SELECT jobId 
                               FROM usersoffers 
                               WHERE userId = $userId)";
        
        $conn = connect();
        $result = mysqli_query($conn, $query);
        
        $offers = [];

        while ($row = mysqli_fetch_assoc($result)) {
            $offers[] = $row;
        }
        
        return $offers;
    }
    
    function incrementViewsCount($offerId) {

        $offer = getOfferById($offerId);
        $currentClicks = $offer['clicks'];
        $currentClicks++;
        $query = "UPDATE offers
                  SET clicks = $currentClicks
                  WHERE id = $offerId";
        
        $conn = connect();
        $result = mysqli_query($conn, $query);
        
        return $result;

    }

    function getSearchResults($jobTitle, $category, $city) {
        
        if ($city != null && $jobTitle != null) {
            $query = "SELECT * 
                      FROM offers 
                      WHERE city LIKE '%$city%' 
                            AND jobTitle LIKE '%jobTitle'";
        } else if ($city != null) {
            $query = "SELECT * 
                    FROM offers 
                    WHERE city LIKE '%$city%'";
        } else if ($jobTitle != null) {
            $query = "SELECT * 
                      FROM offers 
                      WHERE jobTitle LIKE '%$jobTitle%'";
        } else if ($category != null) {
            $query = "SELECT * 
                      FROM offers 
                      WHERE category LIKE '$category'";
        } else {
            $query = "SELECT * 
                      FROM offers 
                      WHERE jobTitle LIKE '%$jobTitle%' 
                            OR city LIKE '%$city%'
                            OR category LIKE '$category'";
        }

        $conn = connect();
        $result = mysqli_query($conn, $query);

        $offers = [];

        while ($row = mysqli_fetch_assoc($result)) {
            $offers[] = $row;
        }
        
        return $offers;

    }

    function getTopViewedOffer() {
        $query = "SELECT *
                  FROM offers
                  WHERE clicks = (SELECT max(clicks) 
                                  FROM offers)";
        $conn = connect();
        $result = mysqli_query($conn, $query);
        
        $offer = mysqli_fetch_assoc($result);

        return $offer;
    }
?>