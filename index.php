<?php
    session_start();
    $BASE_URL = ".";
    require "$BASE_URL/database/db.php";
    require "$BASE_URL/data/jobOffers.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="styles/style.css">
    <script type="text/javascript">
		
		function loadXMLData(){

			var httpRequest = new XMLHttpRequest();

			httpRequest.onreadystatechange = function(){
				if (httpRequest.readyState == 4 && httpRequest.status == 200){
					document.getElementById("ajax-offers").innerHTML = httpRequest.responseText;
				}
			}

			httpRequest.open("Get", "pages/ajaxOffers.php", true);
			httpRequest.send();
		}


		setInterval(loadXMLData, 5000);
	</script>
</head>
<body>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0"></script>
    <?php
        require 'navbar.php';
    ?>
    <form action="">
        <div class="search-bar">
            <div class="left">
                <span>
                    <input name="job-title" type="text" placeholder="Search By Job Title ...">
                </span>
                <span class="middle-borders">
                    <select name="category" class="left-border">
                        <option value="" disabled selected>Category</option>
                        <option value="part-time">Part-time</option>
                        <option value="full-time">Full-time</option>
                    </select>
                </span>
                <span>
                    <input name="city" type="text" placeholder="Search By City ...">
                </span>
                <input id="submit" type="submit" value="Search">
            </div>
        </div>
    </form>
    <section>
        <?php
            if(isset($_GET['job-title']) OR isset($_GET['city']) OR isset($_GET['category'])){
                if (!isset($_GET['category'])) {
                    $offers = getSearchResults($_GET['job-title'], 'time', $_GET['city']);
                } else {
                    $offers = getSearchResults($_GET['job-title'], $_GET['category'], $_GET['city']);
                }
        ?>
        <div class="sponsored search-container">
            <h1>Search Results</h1>
            <h4>Number of Results = <?php echo (string)sizeof($offers);?></h4>
            <div class="offers-container search-offers">
                <?php
                    foreach ( $offers as $offer ){
                ?>
                        <div class="element-container">
                            <div class="element">
                                <div class="image">
                                    <img alt="job-image" src="<?php
                                        echo $offer['photo']
                                    ?>">
                                </div>
                                <div class="text">
                                    <div class="job-title">
                                        <?php
                                            echo $offer['jobTitle'];
                                        ?>
                                    </div>
                                    <div class="details">
                                        <a href=<?php echo '"pages/details.php?id=' . $offer['id'] . '"';?>>
                                            Click for details
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php
                    }
                ?>
            </div>
        </div>
        <?php
            }
        ?>

        <div class="sponsored">
            <h1>Sponsered Offers</h1>
            <div id="ajax-offers" class="offers-container">
            </div>
        </div>
        <div class="all-offers">
            <h1>All Job Offers</h1>
            <div class="horizental-container">
                <div class="all-offers-container">
                    <?php
                        $query = "SELECT * 
                                FROM offers 
                                WHERE status = 'approved' 
                                ORDER BY postingTime DESC";
                        $result = mysqli_query($conn, $query);
                        while ($row = mysqli_fetch_array($result)){
                    ?>
                            <div class="element-container">
                                <div class="element">
                                    <div class="image">
                                        <img alt="job-image" src="<?php
                                            echo $row['photo']
                                        ?>">
                                    </div>
                                    <div class="text">
                                        <div class="job-title">
                                            <?php
                                                echo $row['jobTitle'];
                                            ?>
                                        </div>
                                        <div class="details">
                                            <a href=<?php echo '"pages/details.php?id=' . $row['id'] . '"';?>>
                                                Click for details
                                            </a>
                                        </div>
                                    </div>
                                    <div class="fb-like-container">
                                        <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="true"></div>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }
                    ?>
                </div>
                <div class="most-viewed-offer">
                    <div><h3>Most Viewed</h3></div>
                    <?php 
                        $offer = getTopViewedOffer();
                        if(!empty($offer)) {
                    ?>
                        <div class="image-holder">
                            <img src=<?php echo $offer['photo']; ?> alt="">
                        </div>
                        <h4><?php echo $offer['jobTitle']; ?></h4>
                        <div>
                            <a href=<?php echo '"pages/details.php?id=' . $offer['id'] . '"';?>>Click to view offer</a>
                        </div>
                    <?php
                        }
                    ?>
                </div>
            </div>

        </div>
    </section>
</body>
</html>