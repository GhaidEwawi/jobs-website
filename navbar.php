<nav>
    <div class="right"><span><a href=<?php echo $BASE_URL; ?>>Palestine Jobs</a></span></div>
    <div class="left">
        <?php
            require "database/connection.php";
            if (!isset($_SESSION['logged-in'])){
                echo "<span><a href='" . $BASE_URL . "/pages/login.php'>Log In</a></span>";

            } else {
                echo "<span><a href='" . $BASE_URL . "/pages/addJobOffer.php'>Add Job Offer</a></span>";
                echo "<span><a href='" . $BASE_URL . "/pages/myAnnouncements.php'>My Announcements</a></span>";
                
                if($_SESSION['logged-in-permission'] == "admin") {
                    echo "<span><a href='" . $BASE_URL . "/pages/controlPanel.php'>Control Panel</a></span>";
                }
                echo "<span><a class='logout' href='" . $BASE_URL . "/auth/logout.php'>Log Out</a></span>";
            }
        ?>
    </div>
</nav>