<?php
    session_start();
    require "constants.php";
    require "$BASE_URL/database/db.php";
    require "$BASE_URL/data/jobOffers.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Announcements</title>
    <link rel="stylesheet" href="../styles/style.css">
</head>
<body>
    <?php
        require "$BASE_URL/navbar.php";
    ?>
    <section>
        
        <?php
            $offers = getOffersByUserId($_SESSION['logged-in']);
            if ( !empty($offers) ) {
                foreach ( $offers as $offer ) {
        ?>
                    <div class="offer-element">
                        <h1>
                            <?php echo $offer['jobTitle']; ?>
                        </h1>
                        <div class="details-image-container">
                            <img src=<?php echo $offer['photo']; ?> alt="job-picture">
                        </div>
                        <div class="my-details-text">
                            <div class="details-element">
                                <p>
                                    <span class="key">Company Name :</span>
                                    <span class="value"><?php echo $offer['companyName']; ?></span>
                                </p>
                            </div>
                            <div class="details-element">
                                <p>
                                    <span class="key">Category :</span>
                                    <span class="value"><?php echo $offer['category']; ?></span>
                                </p>
                            </div>
                            <div class="details-element">
                                <p>
                                    <span class="key">Address :</span>
                                    <span class="value"><?php echo $offer['address']; ?></span>
                                </p>
                            </div>
                            <div class="details-element">
                                <p>
                                    <span class="key">City :</span>
                                    <span class="value"><?php echo $offer['city']; ?></span>
                                </p>
                            </div>
                            <div class="details-element">
                                <p>
                                    <span class="key">Street :</span>
                                    <span class="value"><?php echo $offer['street']; ?></span>
                                </p>
                            </div>
                            <div class="details-element">
                                <p>
                                    <span class="key">Details :</span>
                                    <span class="value"><?php echo $offer['details']; ?></span>
                                </p>
                            </div>
                            <div class="details-element">
                                <p>
                                    <span class="key">Job Requirements :</span>
                                    <span class="value"><?php echo $offer['jobRequirements']; ?></span>
                                </p>
                            </div>
                            <div class="details-element">
                                <p>
                                    <span class="key">Salary :</span>
                                    <span class="value"><?php echo $offer['salary']; ?></span>
                                </p>
                            </div>
                            <div class="details-element">
                                <p>
                                    <span class="key">TelNo :</span>
                                    <span class="value"><?php echo $offer['telNo']; ?></span>
                                </p>
                            </div>
                            <div class="details-element">
                                <p>
                                    <span class="key">mail :</span>
                                    <span class="value"><?php echo $offer['email']; ?></span>
                                </p>
                            </div>
                        </div>
                        <h2>
                        <a href=<?php echo '"editableDetails.php?id=' . $offer['id'] . '"';?>>Edit Offer</a>
                        </h2>
                    </div>
        <?php
                }
            } else {
                echo "You don't have any offers to display";
            }
        ?>
    </section>
</body>
</html>