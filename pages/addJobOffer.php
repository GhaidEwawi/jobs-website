<?php
    session_start();
    require "constants.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../styles/style.css">
</head>
<body>
    <?php
        require "$BASE_URL/navbar.php";
    ?>

    <?php
		if(isset($_GET['msg'])){
            if($_GET['msg'] == 'success')
                echo '<h1>User Added Successfully</h1>';
		}
	?>

    <form action="../data/addJobOffer.php" method="POST">
        <h1>Job Offer Information</h1>
        <div class="form-container">
            <div class="labels">
                <div class="label-container">
                    <label for="jobTitle">Job Title</label>
                </div>
                <div class="label-container">
                    <label for="companyName">Compnay Name</label>
                </div>
                <div class="label-container">
                    <label for="category">Category</label>
                </div>
                <div class="label-container">
                    <label for="address">Address</label>
                </div>
                <div class="label-container">
                    <label for="city">City</label>
                </div>
                <div class="label-container">
                    <label for="street">Street</label>
                </div>
                <div class="label-container">
                    <label for="details">Details</label>
                </div>
                <div class="label-container">
                    <label for="jobRequirements">Job Requirements</label>
                </div>
                <div class="label-container">
                    <label for="salary">Salary</label>
                </div>
            </div>
            <div class="inputs">
                <div class="input-container">
                    <input required type="text" name="jobTitle">
                </div>
                <div class="input-container">
                    <input required type="text" name="companyName">
                </div>
                <div class="input-container">
                    <select required name="category">
                        <option value="part-time">Part Time</option>
                        <option value="full-time">Full Time</option>
                    </select>
                </div>
                <div class="input-container">
                    <input required type="address" name="address">
                </div>
                <div class="input-container">
                    <input required type="text" name="city">
                </div>
                <div class="input-container">
                    <input required type="text" name="street">
                </div>
                <div class="input-container">
                    <textarea required type="text" name="details"></textarea>
                </div>
                <div class="input-container">
                    <textarea required name="jobRequirements"></textarea>
                </div>
                <div class="input-container">
                    <input required type="text" name="salary">
                </div>
            </div>
        </div>
        <br>
        <h1>Contact Information</h1>
        <div class="form-container">
            <div class="labels">
                <div class="label-container">
                    <label for="telNo">TelNo</label>
                </div>
                <div class="label-container">
                    <label for="email">Email</label>
                </div>
                <div class="label-container">
                    <label for="photo">Photo Link</label>
                </div>
            </div>
            <div class="inputs">
                <div class="input-container">
                    <input required placeholder="XXX-XXX-XXXX" type="tel" name="telNo">
                </div>
                <div class="input-container">
                    <input required type="email" name="email">
                </div>
                <div class="input-container">
                    <input required type="text" name="photo">
                </div>
            </div>
        </div>
        <button type="submit">Submit</button>
    </form>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script src="http://css-tricks.com/examples/TextareaTricks/js/autoresize.jquery.min.js"></script>
    <script>
        $('textarea').autoResize();
    </script>
</body>
</html>