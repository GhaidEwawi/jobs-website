<?php
    session_start();
    require "constants.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../styles/style.css">
</head>
<body>
    <?php
        require "$BASE_URL/navbar.php";
        require "$BASE_URL/database/db.php";
        require "$BASE_URL/data/jobOffers.php";
    ?>
    
    <div class="section">
        <?php
            if(isset($_GET['msg'])){
                if($_GET['msg'] == 'success'){
                    echo '<h1 class="alert alert-success">Offer Updated Successfully</h1>';
                }
            }
        ?>
        <form action="../data/updateJobOffer.php" method="POST">
            <?php
                require "$BASE_URL/database/connection.php";
                
                $id = $_GET['id'];
                $offer = getOfferById($id);
            ?>
            <input hidden name="id" value=<?php echo "$id"; ?>>

            <h1>
                <?php echo $offer['jobTitle']; ?>
            </h1>
            <div class="details-image-container">
                <img src=<?php echo $offer['photo']; ?> alt="job-picture">
            </div>
            <div class="editable-details-text">
                <div class="details-element">
                    <p>
                        <span class="key">Job Title :</span>
                        <span class="value"><input name="jobTitle" value=<?php echo $offer['jobTitle']; ?>></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">Company Name :</span>
                        <span class="value"><input name="companyName" value=<?php echo $offer['companyName']; ?>></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">Category :</span>
                        <span class="value">
                            <select required name="category">
                                <option value="part-time">Part Time</option>
                                <option value="full-time">Full Time</option>
                            </select>
                        </span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">Address :</span>
                        <span class="value"><input name="address" value=<?php echo $offer['address']; ?>></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">City :</span>
                        <span class="value"><input name="city" value=<?php echo $offer['city']; ?>></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">Street :</span>
                        <span class="value"><input name="street" value=<?php echo $offer['street']; ?>></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">Details :</span>
                        <span class="value">
                            <textarea name="details"><?php echo $offer['details']; ?></textarea>
                        </span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">Job Requirements :</span>
                        <span class="value">
                            <textarea name="jobRequirements"><?php echo $offer['jobRequirements']; ?></textarea>
                        </span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">Salary :</span>
                        <span class="value"><input name="salary" value=<?php echo $offer['salary']; ?>></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">TelNo :</span>
                        <span class="value"><input name="telNo" value=<?php echo $offer['telNo']; ?>></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">mail :</span>
                        <span class="value"><input name="email" value=<?php echo $offer['email']; ?>></span>
                    </p>
                </div>
            </div>
            <button>Save Changes</button>
        </form>
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script src="http://css-tricks.com/examples/TextareaTricks/js/autoresize.jquery.min.js"></script>
    <script>
        $('textarea').autoResize();
    </script>
</body>
</html>