<?php
    session_start();
    require "constants.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../styles/style.css">
</head>
<body>
    <?php
        require "$BASE_URL/navbar.php";
    ?>
    
    <?php
		if(isset($_GET['msg'])){
            if($_GET['msg'] == 'error'){
                echo '<h1>Couldn\'t Register User, try again.</h1>';
            }
		}
	?>

    <form action="../auth/register.php" method="POST">
        <h1>User Registeration</h1>
        <div class="form-container">
            <div class="labels">
                <div class="label-container">
                    <label for="username">Name</label>
                </div>
                <div class="label-container">
                    <label for="password">Password</label>
                </div>
                <div class="label-container">
                    <label for="telNo">TelNo</label>
                </div>
                <div class="label-container">
                    <label for="address">Address</label>
                </div>
                <div class="label-container">
                    <label for="email">Email</label>
                </div>
                <div class="label-container">
                    <label for="resume">Resume Link</label>
                </div>
                <div class="label-container">
                    <label for="role">Role</label>
                </div>
            </div>
            <div class="inputs">
                <div class="input-container">
                    <input required type="text" name="username">
                </div>
                <div class="input-container">
                    <input required type="password" name="password">
                </div>
                <div class="input-container">
                    <input required type="tel" placeholder="XXX-XXX-XXXX" name="telNo">
                </div>
                <div class="input-container">
                    <input required type="address" name="address">
                </div>
                <div class="input-container">
                    <input required type="email" name="email">
                </div>
                <div class="input-container">
                    <input required type="file" name="resume">
                </div>
                <div class="input-container">
                    <select required name="role" id="">
                        <option value="admin">Admin</option>
                        <option value="seeker">Seeker</option>
                        <option value="recruiter">Recruiter</option>
                    </select>
                </div>
            </div>
        </div>
        <button type="submit">Submit</button>
    </form>
</body>
</html>