<?php
    session_start();
    require "constants.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../styles/style.css">
</head>
<body>
    <?php
        require "$BASE_URL/navbar.php";
    ?>

    <?php
		if(isset($_GET['msg'])){
            if($_GET['msg'] == 'error'){
                echo '<h1 class="alert alert-danger">Incorrect User or Password</h1>';
            } else if ($_GET['msg'] == 'reg') {
                echo '<h1 class="alert alert-success">User Registered Successfully, Log in to continue</h1>';
            }
		}
	?>
    
    <form action="../auth/login.php" method="POST">
            <h1>Login page:</h1>
        <div class="form-container">
            <div class="labels">
                <div class="label-container">
                    <label for="name">Name</label>
                </div>
                <div class="label-container">
                    <label for="password">Password</label>
                </div>
            </div>
            <div class="inputs">
                <div class="input-container">
                    <input type="text" name="username" required>
                </div>
                <div class="input-container">
                    <input type="password" name="password" required>
                </div>
            </div>
        </div>
        <button type="submit">Submit</button>
        <a href="register.php"><h2>Create New User</h2></a>
    </form>
</body>
</html>