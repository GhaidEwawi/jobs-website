<?php
    session_start();
    require "constants.php";
    require "$BASE_URL/database/db.php";
    require "$BASE_URL/data/jobOffers.php";
    require "$BASE_URL/data/users.php";

    if (!isset($_SESSION['logged-in']) || ($_SESSION['logged-in-permission'] != "admin")) {
        header("Location: ../");
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../styles/style.css">
</head>
<body>
    <?php
        require "$BASE_URL/navbar.php";
    ?>
    
    <?php

    $users = getAllUsers();

    if ( !empty($users) ) {

    ?>
        <h1>Users</h1>
    <div class="table-container">

        <table class='table'>
            <tr>
                <th scope='col'>User ID</th>
                <th scope='col'>Username</th>
                <th scope='col'>Password</th>
                <th scope='col'>Role</th>
                <th scope='col'>Telephone Number</th>
                <th scope='col'>Email</th>
                <th scope='col'>Address</th>
                <th scope='col'>Control</th>
                </tr>
        <?php
        foreach ( $users as $user ) {
        ?>
            <tr>

                <td><?php echo $user['id']; ?></td>
                <td><?php echo $user['username']; ?></td>
                <td><?php echo $user['password']; ?></td>
                <td><?php echo $user['role']; ?></td>
                <td><?php echo $user['telNo']; ?></td>
                <td><?php echo $user['email']; ?></td>
                <td><?php echo $user['address']; ?></td>
                <td><a href=<?php echo "$BASE_URL/auth/removeUser.php?id='" . $user['id'] . "'";?>>Delete User</a></td>
        
            </tr>
            <?php
        }
        ?>
        </table>
    </div>

    <?php

    } else {
        echo "<h1>Error Displaying Users</h1>";;
    }

    $offers = getAllJobOffers();

    if ( !empty($offers) ) {
    ?>

        <h1>Job Offers</h1>
        <div class="offers-section">
        <?php
        foreach ( $offers as $offer ) {
        ?>

        <div class="offer-element">
            <h1>
                <?php echo $offer['jobTitle']; ?>
            </h1>
            <div class="details-image-container">
                <img src=<?php echo $offer['photo']; ?> alt="job-picture">
            </div>
            <div class="my-details-text">
                <div class="details-element">
                    <p>
                        <span class="key">Company Name :</span>
                        <span class="value"><?php echo $offer['companyName']; ?></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">Category :</span>
                        <span class="value"><?php echo $offer['category']; ?></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">Address :</span>
                        <span class="value"><?php echo $offer['address']; ?></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">City :</span>
                        <span class="value"><?php echo $offer['city']; ?></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">Street :</span>
                        <span class="value"><?php echo $offer['street']; ?></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">Details :</span>
                        <span class="value"><?php echo $offer['details']; ?></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">Job Requirements :</span>
                        <span class="value"><?php echo $offer['jobRequirements']; ?></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">Salary :</span>
                        <span class="value"><?php echo $offer['salary']; ?></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">TelNo :</span>
                        <span class="value"><?php echo $offer['telNo']; ?></span>
                    </p>
                </div>
                <div class="details-element">
                    <p>
                        <span class="key">mail :</span>
                        <span class="value"><?php echo $offer['email']; ?></span>
                    </p>
                </div>
            </div>
            <h2>
            <a href=<?php echo '"editableDetails.php?id=' . $offer['id'] . '"';?>>Edit Offer</a>
            </h2>
            <?php
            if ($offer['status'] != 'approved') {
                echo "<h2>" . '<a href="../data/approveOffer.php?id=' . $offer['id'] . '">Approve Offer</a></h2>';
            }
            ?>
        </div>

        <?php
        }
        ?>
        </div>
    <?php
    } else {
        echo "<h1>There are no job offers</h1>";;
    }
    ?>

</body>
<html>