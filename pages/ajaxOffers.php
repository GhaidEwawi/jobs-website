<?php
    require "../database/db.php";
    require "..//data/jobOffers.php";
    $sponseredOffers = getSponseredJobOffers();
    foreach ( $sponseredOffers as $offer ){
?>
        <div class="element-container">
            <div class="element">
                <div class="image">
                    <img alt="job-image" src="<?php
                        echo $offer['photo']
                    ?>">
                </div>
                <div class="text">
                    <div class="job-title">
                        <?php
                            echo $offer['jobTitle'];
                        ?>
                    </div>
                    <div class="details">
                        <a href=<?php echo '"pages/details.php?id=' . $offer['id'] . '"';?>>
                            Click for details
                        </a>
                    </div>
                </div>
            </div>
        </div>
<?php
    }
?>